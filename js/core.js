var core = {

    /**
     * @param {Number[]} body
     * @param {Number[]|String} size
     */
    printCanvas: function (body, size, cnvs) {
        if (typeof size === 'string') size = size.split(',');
        cnvs = cnvs || this.createCanvas(size);

        let ctx = this.getContext(cnvs);
        let imgData = ctx.createImageData(size[0], size[1]);

        imgData.data.set(body);

        ctx.putImageData(imgData, 0, 0)

        return [cnvs, ctx, imgData];
    },

    getContext: function(cnvs) {
		if (cnvs._bfContext) return cnvs._bfContext;
		
        return cnvs._bfContext = cnvs.getContext('2d');
    },

    createCanvas: function(size) {
        if (typeof size === 'string') size = size.split(',');

        let cnvs = document.createElement('canvas');
        cnvs.width = size[0];
        cnvs.height = size[1];
        cnvs._bfContext = cnvs.getContext('2d');

        document.body.append(cnvs);

        return cnvs;
    },

    getAbsCoord: function(rect, size) {
        if (rect.px) rect.x = size[0] * rect.px | 0;
        if (rect.py) rect.y = size[1] * rect.py | 0;
        if (rect.pw) rect.w = size[0] * rect.pw | 0;
        if (rect.ph) rect.h = size[1] * rect.ph | 0;

        return rect
    },

    printCanvasBorder: function(ctx, rect, style) {
        ctx.beginPath();
        ctx.rect(rect.x, rect.y, rect.w, rect.h);
        ctx.strokeStyle = style || "#00e2ff";
        ctx.stroke();
    },
	
	printCanvasItem: function(ctx, item, fn) {
		ctx[item.colorField] = item.colorValue;
		if (fn) {
			fn.apply(ctx, item.args);
		} else {
			ctx[item.type](...item.args);
		}
	}
}