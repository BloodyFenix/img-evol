﻿var app = {
    
    canvasList: [],
	npsList: [],
	count: 100,
	diffList: [],
	counter: 0,
	
    init: function() {
		let pause = document.getElementById('pause');
		pause.addEventListener('click', () => {
			this.stop = !this.stop;
			this.next(this._originImgData);
        });
        
        let copySaveStr = document.getElementById('copy-save-str');
        copySaveStr.addEventListener('click', () => {
            let text = this.getSaveStr()
            
            navigator.clipboard.writeText(text).then(
                function() { 
                    console.log('Copy to clipboard:\n' + text);
                }, 
                err => {
                    console.log('Something went wrong', err);
                }
            )
        });
		
		let loadSaveStr = document.getElementById('load-save-str');
        loadSaveStr.addEventListener('click', () => {
            navigator.clipboard.readText().then(
                function(text) { 
                    console.log('Load from clipboard:\n' + text);
					app.loadFromStr(text);
                }, 
                err => {
                    console.log('Something went wrong', err);
                }
            )
        });

        app.infoCounter = document.getElementById('info-counter');
        app.infoRules = document.getElementById('info-rules');
		
        let fileInput = document.getElementsByTagName('input')[0];
        fileInput.addEventListener('change', this._fileChanged.bind(this, fileInput))

		this.itemTypeList = Object.keys(this.itemMap).filter((type) => ! this.itemMap[type].removed);
		
        this.initCanvas();
		this.initNps();
    },

    initCanvas: function() {
		this._originCnvs = document.createElement('canvas');
        this._originCnvs.classList.add('canvas', 'canvas_origin');
        document.body.appendChild(this._originCnvs);
			
        for (let i = 0; i < this.count; i++) {
            let cnvs = document.createElement('canvas');
            cnvs.classList.add('canvas', 'canvas_' + i);
            document.body.appendChild(cnvs);
            this.canvasList.push(cnvs);
        }
    },
	
	initNps: function() {
		for (let i = 0; i < this.count; i++) {
            this.npsList.push([]);
        }
	},
    
    _fileChanged: function(fileInput) {
        this.getBase64(fileInput.files[0])
            .then(this.setCanvasImage.bind(this));
    },

    getBase64: function(file) {
        return new Promise((resolve, reject) => {
            if (file) {
                var reader = new FileReader();
                reader.addEventListener('load', function(e) { resolve(e.target.result); });
                reader.readAsDataURL(file);
            } else {
                reject();
            }
        });
    },

    setCanvasAttributes: function(w, h) {
		this._originCnvs.setAttribute('width', this._w = w);
		this._originCnvs.setAttribute('height', this._h = h);

        this.canvasList.forEach((cnvs) => {
            cnvs.setAttribute('width', w);
            cnvs.setAttribute('height', h);
        });
    },

    setCanvasImage: function(base64) {
        let img = new Image();
        img.addEventListener('load', () => {
            this.setCanvasAttributes(img.width, img.height);
            let ctx = this._originCnvs._bfContxt = this._originCnvs.getContext('2d');
            ctx.drawImage(img, 0, 0)

            this.start(this._originImgData = ctx.getImageData(0, 0, this._w, this._h));
        });
        img.src = base64;
    },

    start: function(orgnImdData) {
		delete this.stop;
		
        this.next(orgnImdData);
    },
	
	next: function(orgnImdData) {
		if (this.stop) return;
		
		this.counter++;
		
		this.npsList.forEach((nps, i) => {
			i && this.randomizeNps(nps);
			let ctx = core.getContext(this.canvasList[i]);
			ctx.clearRect(0, 0, this._w, this._h);
			this.printNps(nps, ctx);
			this.diffList[i] = this.getDiff(ctx, orgnImdData);
		});
		
		let pos = this.diffList.lastIndexOf(Math.min(...this.diffList));
		let npsWin = this.npsList[pos];
		this.npsList = this.npsList.map(() => npsWin.map((item) => {
			return {
				type: item.type,
				args: item.args.concat(),
				colorField: item.colorField,
				colorValue: item.colorValue
			};
        }));
        
        app.infoCounter.innerText = app.counter;
        app.infoRules.innerText = npsWin.length;
		
		setTimeout(() => this.next(orgnImdData), 10);
	},
	
	getDiff: function(ctx, orgnImdData) {
		let itemImgData = ctx.getImageData(0, 0, this._w, this._h),
			res = 0;
			
		itemImgData.data.forEach((v, i) => {
			res += (Math.abs(orgnImdData.data[i] - v) / 10000);
		});
		
		return res;
	},
	
	printNps: function(nps, ctx) {
		nps.forEach((item) => {
			core.printCanvasItem(ctx, item, this.itemMap[item.type].fn);
		});
	},
	
	colorFieldTypes: ['fillStyle'],
	
	itemMap: {
		fillRect: {
			args: [[0, 'w'], [0, 'h'], [1, 100], [1, 100]],
			colorField: 'fillStyle'
		},
		strokeRect: {
			removed: true,
			args: [[0, 'w'], [0, 'h'], [1, 100], [1, 100]],
			colorField: 'fillstyle'
		},
		bfEllipse: {
			args: [[0, 'w'], [0, 'h'], [0, 'w/2'], [0, 'h/2'], [0, 359]],
			colorField: 'fillStyle',
			fn: function() {
				let [x, y, radiusX, radiusY, rotation] = arguments;
				this.beginPath();
				this.ellipse(x, y, radiusX, radiusY,  Math.PI * rotation / 360, 0, Math.PI * 2);
				this.fill();
			}
		},
		bfEllipse2: {
			args: [[0, 'w'], [0, 'h'], [0, 'w/2'], [0, 'h/2'], [0, 359], [0, 359], [0, 359]],
			colorField: 'fillStyle',
			fn: function() {
				let [x, y, radiusX, radiusY, rotation, startAngle, endAngle] = arguments;
				this.beginPath();
				this.ellipse(x, y, radiusX, radiusY,  Math.PI * rotation / 360, Math.PI * startAngle / 360, Math.PI * endAngle / 360);
				this.fill();
			}
		},
	},
	
	createItemArgs: function(protoArgs) {
		return protoArgs.map((limits) => {
			limits = limits.map((v) => {
				switch (v) {
					case 'w':
						return this._w;
					case 'h':
						return this._h;
					case 'w/2':
						return this._w / 2;
					case 'h/2':
						return this._h / 2;
				}
				
				return v;
			});
			
			return (Math.random() * (limits[1] - limits[0]) | 0) + limits[0];
		});
	},
	
	randomColor: function() {
		return 'rgb(' + (Math.random() * 256 | 0) + ',' + (Math.random() * 256 | 0) + ',' + (Math.random() * 256 | 0) + ')';
	},
	
	_driverAdd: function(nps) {
		let itemType = this.itemTypeList[Math.random() * this.itemTypeList.length | 0],
			protoItem = this.itemMap[itemType];

		nps.push({
			type: itemType,
			args: this.createItemArgs(protoItem.args),
			colorField: protoItem.colorField,
			colorValue: this.randomColor()
		});
	},
	
	_driverEdit: function(nps) {
		var item = nps[Math.random() * nps.length | 0];
		if (Math.random() > 0.5) {
			// if (! this.itemMap[item.type]) {
				// this._driverRemove(nps, item);
				// return;
			// }
			item.args = this.createItemArgs(this.itemMap[item.type].args);
		} else {
			item.colorValue = this.randomColor();
		}
	},
	
	_driverRemove: function(nps, item) {
		let i = Math.random() * nps.length | 0;
		if (item) i = nps.indexOf(item);
		nps.splice(i, 1);
	},
	
	randomizeNps: function(nps) {
		let drivers = [this._driverAdd];
		
		if (nps.length) drivers.push(this._driverEdit, this._driverEdit, this._driverRemove);
		
		drivers[Math.random() * drivers.length | 0].call(this, nps);
	},
	
	getSaveStr: function() {
		let allTypes = Object.keys(this.itemMap);
		let nps = this.npsList[0];
		return JSON.stringify(nps.map(
			(item) => [allTypes.indexOf(item.type), item.args, this.colorFieldTypes.indexOf(item.colorField), item.colorValue.replace(/[a-z()]*/g, '')])
		);
	},
	
	loadFromStr: function(str) {
		let allTypes = Object.keys(this.itemMap);
		this.npsList[0] = JSON.parse(str).map((item) => {
			return { type: allTypes[item[0]], args: item[1], colorField: this.colorFieldTypes[item[2]], colorValue: 'rgb(' + item[3] + ')' };
		});
	}
};

app.init();